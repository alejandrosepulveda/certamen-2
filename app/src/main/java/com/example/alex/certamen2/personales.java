package com.example.alex.certamen2;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class personales extends Fragment {

public Button botonsi;
    public EditText nombre,edad;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_personales, container, false);

        final EditText nombreid = view.findViewById(R.id.nombreid);

        botonsi = (Button)view.findViewById(R.id.botonsi);
        botonsi.setOnClickListener( new View.OnClickListener() {

            public void onClick(View view){

                if(!nombreid.getText().toString().isEmpty()){


                    Fragment nuevoFragment = new contacto();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content_frame, nuevoFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();

                }else{

                    Snackbar.make(view, "Campo nombre no puede quedar vacio", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }






            }

        });

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("personales");
    }

}
