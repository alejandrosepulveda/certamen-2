package com.example.alex.certamen2;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class inicio extends Fragment {
    public Button iniciar;
    public EditText contraseña,nombre;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_inicio, container, false);
        final EditText contraseña = view.findViewById(R.id.contraseña);
        final EditText nombre = view.findViewById(R.id.nombre);

        iniciar = (Button)view.findViewById(R.id.iniciar);
        iniciar.setOnClickListener( new View.OnClickListener() {

            public void onClick(View view) {
                String val2 = (String) nombre.getText().toString();
                String val = (String) contraseña.getText().toString();
                Toast.makeText(getContext(), "Iniciaste sesion con nombre: " + val2, Toast.LENGTH_LONG).show();
                Toast.makeText(getContext(), "Con contraseña: " + val, Toast.LENGTH_LONG).show();

            }
        });



        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("inicio");
    }



}
